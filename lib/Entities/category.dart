class Category{
  int id;
  String name;
  Category(this.id, this.name);

  static List<Category> getCategory(){
    return <Category>[
      Category(1,"Book"),
      Category(2,"Pen"),
      Category(3,"Erase"),
    ];
  }
}