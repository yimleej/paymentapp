import 'package:flutter/material.dart';
import 'package:paymentapp/pages/list_Unit_page.dart';
import 'package:paymentapp/pages/list_categories_page.dart';
import 'package:paymentapp/pages/payment_page.dart';

class Menu extends StatelessWidget {
  final String title;
  final IconData icon;
  final int index;
  Menu({this.title, this.icon, this.index});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blue,
      child: InkWell(
        onTap: () {
          if (this.index == 0) {
            Navigator.push(context,
              MaterialPageRoute(builder: (context)=> PaymentPage()),
            );
          } else if (this.index == 1) {
            print("History");
          } else if (this.index == 2) {
            Navigator.push(context,
              MaterialPageRoute(builder: (context)=> CategoryPage()),
            );
          } else if (this.index == 3) {
            Navigator.push(context,
              MaterialPageRoute(builder: (context)=> UnitPage()),
            );
          } else {
            print("Setting");
          }
        },
        child: Center(
          child: Column(
            children: <Widget>[
              Icon(icon, size: 150, color:Colors.white,),
              Text(title, style: TextStyle(color: Colors.white, fontSize: 17),)
            ],
          ),
        ),
      ),
    );
  }
}
