import 'package:flutter/material.dart';

class HelpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(child: Text("Help")),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "Need a help please Connect:",
            style: TextStyle(fontSize: 17, color: Colors.green,fontWeight: FontWeight.bold),
          ),
          SizedBox( height: 10,),
          Text(
            "Whatsapp: 02059574310",
            style: TextStyle(color: Colors.blue),
          ),
          SizedBox( height: 10,),
          Text(
            "Phone number: 0309646476",
            style: TextStyle(color: Colors.blue),
          ),
          SizedBox(height: 10,),
          Text(
            "Email: korlakanh2020@gmail.com",
            style: TextStyle(color: Colors.blue),
          )
        ],
      ),
    );
  }
}
