import 'package:flutter/material.dart';
import 'package:paymentapp/Entities/category.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}
//List<String> categories = ["Book","computer"];
String _selected='';
class _PaymentPageState extends State<PaymentPage> {
  final format = DateFormat("yyyy-MM-dd");
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment Page"),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 10, left: 30, right: 30),
        child: SingleChildScrollView(
          child: Column (
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Description",
                  labelStyle: TextStyle(fontSize: 20)
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: DropdownButton(
                  value: _selected,
                  isExpanded: true,
                  onChanged: (String newValue) {
                    _selected = newValue;
                  },
                  hint: Text("Select Category"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Align(
                  alignment: Alignment.centerLeft ,
                  child: Text('Date',style: TextStyle(fontSize: 17,color: Colors.black54)),
                ),
              ),
              DateTimeField(
                format: format,
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(1900),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "Price",
                    labelStyle: TextStyle(fontSize: 17),
                  ),
                  keyboardType: TextInputType.number,
                ),
              ),
              SizedBox(height: 50,),
              RaisedButton(
                child: Text("Save", style: TextStyle(fontSize: 17, color: Colors.white),),
                color: Colors .blue,
                onPressed: (){},
                hoverColor: Colors.red,
                focusColor: Colors.pinkAccent,
              )
            ],
          ),
        ),
      ),
    );
  }
}
