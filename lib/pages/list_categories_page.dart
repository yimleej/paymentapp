import 'package:flutter/material.dart';

import 'category_details_page.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Category Page"),
        actions: <Widget>[
          IconButton(
            icon:Icon(
              Icons.add_circle_outline,
              color: Colors.white,
              size:30,
            ),
            onPressed: (){
              _showCategoryDetailsPage();
            },
          )
        ],
      ),
      body: Container(
        child: ListView(
         children: <Widget>[
           ListTile(
             title: Text("Book"),
             leading: Icon(Icons.book, color: Colors.blue,),
           ),
           ListTile(
             title: Text("com"),
             leading: Icon(Icons.book,color: Colors.blue,),
           )
         ],
        ),
      ),
    );
  }
  void _showCategoryDetailsPage(){
    showDialog(context: context,
      builder: (BuildContext context){
      return CategoryDetailsPage();
      }
    );
  }
}
