import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("About"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "Payment App Test",
            style: TextStyle(fontSize: 20, color: Colors .black, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10,),
          Text(
            "Verson 1.0.10",
          ),
          SizedBox(height: 10,),
          Text(
            "   Payment App was developed on February 20 2020, it is developed by Mr.Korlakanh Moua. This app has performent to use for many fuctions."
          )
        ],
      ),
    );
  }
}

