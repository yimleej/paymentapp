import 'package:flutter/material.dart';
import 'package:paymentapp/pages/change_passowrd_page.dart';
import 'package:paymentapp/pages/register_page.dart';

import 'about_page.dart';
import 'help_page.dart';

class DrawerPage extends StatefulWidget {
  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountEmail: Text("kor2020@gmail.com"),
            accountName: Text("Korlakanh"),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset("assets/images/User.png"),
            ),
          ),
          ListTile(
            title: Text(
              "Change Password",
              style: TextStyle(fontSize: 17,color: Colors.black),),
          leading: Icon(
           Icons.vpn_key,
            color: Colors.blue,
            ),
            onTap: (){
              _showChangePassword();
            },
          ),
          Divider(
            height: 1,
            color: Colors.blue ,
          ),
          ListTile(
            title: Text(
              "Register",
              style: TextStyle(fontSize: 17,color: Colors.black),),
            leading: Icon(
              Icons.supervisor_account ,
              color: Colors.blue,
            ),
            onTap: (){
              _showRegister();
            },
          ),
          Divider(
            height: 1,
            color: Colors.blue ,
          ),
          ListTile(
            title: Text(
              "About",
              style: TextStyle(fontSize: 17,color: Colors.black),),
            leading: Icon(
              Icons.info,
              color: Colors.blue,
            ),
            onTap: (){
            _showAbout() ;
            },
          ),
          Divider(
            height: 1,
            color: Colors.blue ,
          ),
          ListTile(
            title: Text(
              "Help",
              style: TextStyle(fontSize: 17,color: Colors.black),),
            leading: Icon(
              Icons.help ,
              color: Colors.blue,
            ),
            onTap: (){
             _showHelp();
            },
          ),
          Divider(
            height: 1,
            color: Colors.blue ,
          ),
        ],
      ) ,
    );
  }
  void _showChangePassword(){
    showDialog(
        context: context,
    builder:(BuildContext context){
      return ChangePasswordPage();
    });
  }
  void _showRegister(){
    showDialog(
        context: context,
            builder: (BuildContext context){
          return RegisterPage();
    }
    );
  }
  void _showAbout(){
    showDialog(context: context,
      builder: (BuildContext context){
      return AboutPage();
      }
    );
  }
  void _showHelp(){
    showDialog(context: context,
      builder: (BuildContext context){
      return HelpPage();
      }
    );
  }
}

