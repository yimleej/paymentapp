import 'package:flutter/material.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Change Password"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(hintText: "Old Password"),
          ),
          TextFormField(
            decoration: InputDecoration(hintText: "New Password"),
          ),
          TextFormField(
            decoration: InputDecoration(hintText: "Confirm New Password"),
          ),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
            onPressed: null,
            child: Text(
              "Cancel",
              style: TextStyle(fontSize: 17, color: Colors.blue),
            )),
        new FlatButton(
            onPressed: null,
            child: Text(
              "Save",
              style: TextStyle(fontSize: 17, color: Colors.blue),
            ))
      ],
    );
  }
}
