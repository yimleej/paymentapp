import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:paymentapp/pages/home_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isSelected = false;
  String _password;
  TextEditingController txtUsername = TextEditingController();
  TextEditingController txtPassword = TextEditingController();
  void fetchData(String name) async {
    String url = "http://192.168.8.100:8080/payment/dbpassword.php?username=$name";
    final response = await http.get(url);
    if (response.statusCode == 200) {
      //200 is succesfully
      setState(() {
        _password = json.decode(response.body);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            "Login",
            style: TextStyle(color: Colors.white),
//            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(top: 50, bottom: 50, right: 20, left: 20),
            child: Image.asset("assets/images/User.png"),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 30, left: 30, right: 30, bottom: 20),
            child: TextFormField(
              controller: txtUsername,
              decoration: InputDecoration(
                labelText: "Username",
                prefixIcon: Icon(
                  Icons.person,
                  color: Colors.blue,
                ),
                labelStyle: TextStyle(fontSize: 17),
                enabledBorder: OutlineInputBorder(
                  borderSide: new BorderSide(width: 1, color: Colors.blue),
                  borderRadius: new BorderRadius.circular(20),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: new BorderSide(width: 2, color: Colors.blue),
                  borderRadius: new BorderRadius.circular(20),
                ),
              ),

            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 10, left: 30, right: 30, bottom: 0),
            child: TextFormField(
              controller: txtPassword,
              obscureText: true,
              decoration: InputDecoration(
                labelText: "Password",
                prefixIcon: Icon(
                  Icons.vpn_key,
                  color: Colors.blue,
                ),
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: Colors.blue,
                  ),
                  onPressed: () {},
                ),
                labelStyle: TextStyle(fontSize: 17),
                enabledBorder: OutlineInputBorder(
                  borderSide: new BorderSide(width: 1, color: Colors.blue),
                  borderRadius: new BorderRadius.circular(20),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: new BorderSide(width: 2, color: Colors.blue),
                  borderRadius: new BorderRadius.circular(20),
                ),
              ),


            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 0, left: 10, right: 0, bottom: 20),
            child: CheckboxListTile(
              value: _isSelected,
              title: Text(
                "remember me",
                textAlign: TextAlign.left,
              ),
              controlAffinity: ListTileControlAffinity.leading,
              onChanged: (bool value) {
                setState(() {
                  _isSelected = value;
                });
              },
            ),
          ),
          RaisedButton(
            child: Text(
              "Login",
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.blue,
            onPressed: () {
             // fetchData(txtUsername.text);
              if (txtPassword.text == _password) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                );
              } else {
                Alert(
                    context: context,
                    type: AlertType.error,
                    title: "Incorrect your email or password",

                    buttons: [
                      DialogButton(
                        child: Text(
                          "OK",
                          style: TextStyle(fontSize: 25, color: Colors.black),
                        ),
                        onPressed: () => Navigator.pop(context),
                        width: 120,
                      )
                    ]).show();
              }
            },
          ),
          FlatButton(
            child: Text(
              "Register",
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () {},
          )
        ],
      )),
    );
  }
}
