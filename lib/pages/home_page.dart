import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:paymentapp/menus/Menu.dart';
import 'package:paymentapp/pages/Drawer_Page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPress,
      child: new Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
      ),
      drawer: DrawerPage(),
      body: Container(
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            Menu(
              title: "Payment",
              icon: Icons.payment,
              index: 0,
            ),
            Menu(
              title: "History",
              icon: Icons.history,
              index: 1
            ),
            Menu(
              title: "Category",
              icon: Icons.category,
              index: 2,
            ),
            Menu(
              title: "Unit",
              icon: Icons.ac_unit,
              index: 3,
            ),
            Menu(
              title: "Setting",
              icon: Icons.settings,
              index: 4,
            ),
          ],
        ),
      ),
      )
    );
  }
  Future<bool> _onBackPress(){
    return showDialog(
      context: context,
      builder: (context)=>new AlertDialog(
        title: new Text("Do you want back?"),
        actions: <Widget>[
          FlatButton(
            child: Text("No"),
            onPressed: (){
              Navigator.of(context).pop(false);
            },
          ),
          SizedBox(height: 16,),
          FlatButton(
            child: Text("Yes"),
            onPressed: ()=>SystemNavigator.pop(),
          ),
          SizedBox(height: 16,),
        ],
      )
    );
  }
}
