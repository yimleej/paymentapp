import 'package:flutter/material.dart';

class CategoryDetailsPage extends StatefulWidget {
  @override
  _CategoryDetailsPageState createState() => _CategoryDetailsPageState();
}

class _CategoryDetailsPageState extends State<CategoryDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text("Category Form"),
        content: SingleChildScrollView(
          child: Column(
           children: <Widget>[
             TextFormField(
               decoration: InputDecoration(
                 hintText: "CategoryID",
                 hintStyle: TextStyle(fontSize: 17),
               ),
             ),
             TextFormField(
               decoration: InputDecoration(
                 hintText: "CategoryName",
                 hintStyle: TextStyle(fontSize: 17),
               ),
             ),
             Divider(
               height: 2,
               color: Colors.blue,
             ),
           ],
          ),
        ),
      actions: <Widget>[
        new FlatButton.icon(
            onPressed: null,
            icon: Icon(Icons .cancel, color: Colors .red,),
            label: Text ("Cancel", style: TextStyle(fontSize: 17, color: Colors.black),),
        ),
        new FlatButton.icon(
            onPressed: null  ,
            icon: Icon(Icons .save , color: Colors.blue,),
            label: Text("Save", style: TextStyle(fontSize: 17, color: Colors.blue),),
        )
      ],
    );

  }
}
