import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text("Register"),
        content: SingleChildScrollView(
          child: Column(
           children: <Widget>[
             TextFormField(
               decoration: InputDecoration(
                 hintText: "Username",
                 hintStyle: TextStyle(fontSize: 17),
                 prefixIcon: Icon(Icons.person, color: Colors.blue,),
               ),
             ),
             Divider(
               height: 2,
               color: Colors.blue,
             ),
             TextFormField(
               decoration: InputDecoration(
                 hintText: "Password",
                 hintStyle: TextStyle(fontSize: 17),
                 prefixIcon: Icon(Icons.vpn_key, color: Colors.blue,),
               ),
             ),
             Divider(
               height: 2,
               color: Colors.blue,
             ),
             TextFormField(
               decoration: InputDecoration(
                 hintText: "Comfirm Password",
                 hintStyle: TextStyle(fontSize: 17),
                 prefixIcon: Icon(Icons.vpn_key, color: Colors.blue,),
               ),
             ),
             TextFormField(
               decoration: InputDecoration(
                 hintText: "Email",
                 hintStyle: TextStyle(fontSize: 17),
                 prefixIcon: Icon(Icons .email, color: Colors .blue,),
               ),
             ),
             Divider(
               height: 2,
               color: Colors .blue,
             ),
           ],
          ),
        ),
      actions: <Widget>[
        FlatButton.icon(
            onPressed: null,
            icon: Icon(Icons .cancel, color: Colors .red,),
            label: Text ("Cancel", style: TextStyle(fontSize: 17, color: Colors.black),),
        ),
        FlatButton.icon(
            onPressed: null  ,
            icon: Icon(Icons .save , color: Colors.blue,),
            label: Text("Register", style: TextStyle(fontSize: 17, color: Colors.blue),),
        )
      ],
    );

  }
}
