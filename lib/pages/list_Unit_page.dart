import 'package:flutter/material.dart';
import 'package:paymentapp/pages/unit_details_page.dart';

import 'category_details_page.dart';

class UnitPage extends StatefulWidget {
  @override
  _UnitPageState createState() => _UnitPageState();
}

class _UnitPageState extends State<UnitPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Unit Page"),
        actions: <Widget>[
          IconButton(
            icon:Icon(
              Icons.add_circle_outline,
              color: Colors.white,
              size:30,
            ),
            onPressed: (){
              _showUnitDetailsPage();
            },
          )
        ],
      ),
      body: Container(
        child: ListView(
         children: <Widget>[
           ListTile(
             title: Text("Book"),
             leading: Icon(Icons.book, color: Colors.blue,),
           ),
           ListTile(
             title: Text("com"),
             leading: Icon(Icons.book,color: Colors.blue,),
           )
         ],
        ),
      ),
    );
  }
  void _showUnitDetailsPage(){
    showDialog(context: context,
      builder: (BuildContext context){
      return UnitDetailsPage();
      }
    );
  }
}
